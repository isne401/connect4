#include<iostream>
using namespace std;
#include <stdlib.h> 
#include <time.h>
void showgrid(int [6][7]);
void userturn(int [6][7]);
int win(int[6][7]);
void aiturn(int[6][7]);
int main()
{
	int gamewin = -1;//-1=undone 0=draw 1=aiwin 2=userwin
	int grid[6][7] = {	 0,0,0,0,0,0,0
						,0,0,0,0,0,0,0
						,0,0,0,0,0,0,0
						,0,0,0,0,0,0,0
						,0,0,0,0,0,0,0
						,0,0,0,0,0,0,0 };//0 = empty 1=ai 2=user
	cout << "          WELCOME TO CONNECT4\n----------------------------------------------------------";
	srand(time(NULL));
	int start = 0; //1ai start ,2user start
	start = rand() % 2 + 1;
	//if (start == 1)
	//{
	//	cout << "\n MY TURN FIRST ";
	//}
	//else { cout << "\nYOUR TURN FIRST "; }

	while (gamewin == -1)
	{
		cout << endl;
		showgrid(grid);
		userturn(grid);
		gamewin = win(grid);
		if (gamewin == -1)
		{
			aiturn(grid);
			gamewin = win(grid);
		}
	}
	if (gamewin == 0)
	{
		cout << "DRAW";
	}
	else
	{
		if (gamewin == 1)
			cout << "Me";
		else if (gamewin == 2)
			cout << "You";
		cout << "_______________________________________";
		cout  << " win\n";
		showgrid(grid);
	}
	int c;
	cin >> c;
}

void showgrid(int a[6][7])
{
	for (int i = 0; i < 6; i++)
	{
		cout << "|";
		for (int j = 0; j < 7; j++)
		{
			if (a[i][j] == 1)
				cout << "X";
			else if (a[i][j] == 2)
				cout << "O";
			else cout << " ";
			cout << "|";
		}
		cout << endl;
	}
}
void userturn(int a[6][7])
{
	int input=-1;
	while (input < 1 || input>7)
	{
		cout << "which collum do you want to go (1 to 7)";
		cin >> input;
		input -= 1;
		if (a[0][input] != 0&&input>=0&&input<=6)
			{
				input = 9;
				cout << "Collum is full \n";
			}
		input += 1;
	}
	input -= 1;
	for (int i = 6; i >= 0; i--)
	{
		if (a[i][input] == 0)
		{
			a[i][input] = 2;
			return;
		}
	}
	
}
int win(int a[6][7])
{
	
		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (a[i][j] != 0)
				{	
					if (a[i][j] == a[i + 1][j] && a[i+1][j] == a[i + 2][j] && a[i+2][j] == a[i + 3][j] )
						return a[i][j];
				}
			}
		}
		for (int j = 0; j < 7; j++)
		{
			for (int i = 0; i < 4; i++)
			{
				if (a[j][i] != 0)
				{
					if (a[j][i] == a[j ][i+1] && a[j ][i+1] == a[j][i+2] && a[j ][i+2] == a[j][i+3])
						return a[j][i];
				}
			}
		}

		for (int i = 6; i >= 4; i--)
		{
			for (int j = 0; j <= 3; j++)
			{
				if(a[i][j]!=0)
				if (a[i][j] == a[i -1][j + 1] && a[i][j] == a[i - 2][j + 2] && a[i][j] == a[i - 3][j + 3])
				{
					return a[i][j];
				}
			}
		}
		for (int i = 6; i >= 4; i--)
		{
			for (int j = 6; j >= 3; j--)
			{
				if (a[i][j] != 0)
				if (a[i][j] == a[i - 1][j - 1] && a[i][j] == a[i - 2][j - 2] && a[i][j] == a[i - 3][j - 3])
				{
					return a[i][j];
				}
			}
		}

		for (int i = 6; i >= 4; i--)
		{
			for (int j = 6; j >= 3; j--)
			{
				if (a[i][j] == 0)
					return -1;
			}
		}
		return 0;
}

void aiturn(int a[6][7])
{
	for (int j = 0; j < 7; j++)
	{
		for (int i = 3; i >= 0; i--)
		{
			if (a[i][j] != 0)
			{
				if (a[i][j] == a[i + 1][j] && a[i][j] == a[i + 2][j])
				{
					if(a-1>=0)
					if(a[i-1][j]==0){
						a[i - 1][j] = 1;
						return;
					}
				}
			}
		}
	}
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 4; j++)
		{
			int one = 0, two = 0;
			if (a[i][j] == 1)
				one += 1;
			else if (a[i][j] == 2)
				two += 1;
			
			if (a[i][j + 1] == 1)
				one += 1;
			else if (a[i][j + 1] == 2)
				two += 1;
			if (a[i][j + 2] == 1)
				one += 1;
			else if (a[i][j + 2] == 2)
				two += 1;
			if (a[i][j + 3] == 1)
				one += 1;
			else if (a[i][j + 3] == 2)
				two += 1;
			if (one == 3 || two == 3)
			{
				for (int k = 0; k < 4; k++)
				{
					if (a[i][k] == 0)
					{
						a[i][k] = 1;
						return;
					}
				}
			}
		}
	}
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 4; j++)
		{
			int one = 0, two = 0;
			if (a[i][j] == 1)
				one += 1;
			else if (a[i][j] == 2)
				two += 1;

			if (a[i][j + 1] == 1)
				one += 1;
			else if (a[i][j + 1] == 2)
				two += 1;
			if (a[i][j + 2] == 1)
				one += 1;
			else if (a[i][j + 2] == 2)
				two += 1;
			if (a[i][j + 3] == 1)
				one += 1;
			else if (a[i][j + 3] == 2)
				two += 1;
			if ( two == 2)
			{
				for (int k = 0; k < 4; k++)
				{
					if (a[i][k] == 0)
					{
						a[i][k] = 1;
						return;
					}
				}
			}
		}
	}
	int x = rand() % 7;
	int empty = 0;
	while ( empty==0)
	{
		x = rand() % 7;
		if (a[0][x] == 0 )
		{
			empty = 1;
		}
	}
	for (int i = 6; i >= 0; i--)
	{
		if (a[i][x] == 0)
		{
			if (i == 0)
			{
				a[i][x] = 1;
				return;
			}
			else {
				if (a[i + 1][x] != 0)
				{
					a[i][x] = 1;
					return;
				}
			}
		}
	}
}